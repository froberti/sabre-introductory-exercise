import {
    FETCH_PLAYERS,
    FETCH_PLAYERS_REJECTED,
    FETCH_PLAYERS_FULFILLED,
    FILTER_BY
} from './actionTypes';

const initialState = {
    players: [],
    filters: [
        {name:"name",type:"input",inputType:"text",value:""},
        {name:"position",type:"select",options:[{},{},{},{},{},{}],value:""},
        {name:"age",type:"input",inputType:"number",value:""},
    ],
    fetchingPlayers:false,
    fetchPlayersFulfilled: false,
    fetchPlayersRejected: false,
    fetchPlayersError: null
};

export default function reducer(state=initialState,action){
    switch(action.type){
        case FETCH_PLAYERS: return {...initialState,players:state.players,fetchingPlayers:true};
        case FETCH_PLAYERS_REJECTED: return {...state,fetchingPlayers:false,fetchPlayersRejected:true,fetchPlayersError:action.error};
        case FETCH_PLAYERS_FULFILLED: return {...state,fetchingPlayers:false,fetchPlayersFulfilled:true, players: action.payload};
        case FILTER_BY: return {...state, filters: action.payload};
        default: return {...state}
    }
}


//
// export default handleActions({
//     [FETCH_PLAYERS]: (state,action)=> ({...state ,players:state.players}),
//     [FETCH_PLAYERS_REJECTED]: (state,action)=> ({...state,fetchingPlayers:false,fetchPlayersRejected:true,fetchPlayersError:action.error}),
//     [FETCH_PLAYERS_FULFILLED]: (state,action)=> ({...state,fetchingPlayers:false,fetchPlayersFulfilled:true, players: action.payload}),
// },initialState);