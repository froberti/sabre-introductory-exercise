import {
    FETCH_PLAYERS,
    FETCH_PLAYERS_REJECTED,
    FETCH_PLAYERS_FULFILLED,
    SET_FILTERS, FILTER_BY
} from './actionTypes';
import axios from "axios/index";

const fetchPlayers = async (dispatch) => {

    dispatch({type:FETCH_PLAYERS});

    try{
        const footballPlayers = await getFootballPlayers();
        dispatch({type:FETCH_PLAYERS_FULFILLED,payload:footballPlayers});
        return footballPlayers;
    }catch(error){
        dispatch({type:FETCH_PLAYERS_REJECTED,error:error});
        return error;
    }
};

const filterPlayersBy = (filterName,value) =>
    (dispatch,state) =>
        dispatch({type:FILTER_BY,payload:
                state.filters.map(
                    (filter)=>{
                        if(filterName === filter.name)
                            return {[filterName]:value};
                        return filter;
                    })
        });

const getFootballPlayers = () =>
    axios.get("https://football-players-b31f2.firebaseio.com/players.json?print=pretty")
        .then((response)=> response.data)
        .catch((error)=> {throw error});




export {fetchPlayers,filterPlayers}