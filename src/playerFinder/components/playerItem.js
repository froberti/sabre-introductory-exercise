import React from 'react';

const PlayerItem = ({ player }) => (
    <div>
        <div>{player.contractUntil}</div>
        <div>{player.dateOfBirth}</div>
        <div>{player.jerseyNumber}</div>
        <div>{player.name}</div>
        <div>{player.nationality}</div>
        <div>{player.position}</div>
    </div>
);

export default PlayerItem;