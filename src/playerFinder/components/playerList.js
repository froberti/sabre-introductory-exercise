import React from 'react';
import PlayerItem from './playerItem';

const PlayerList = ( {players}) => {
    return(
    <div>
        {/* each player should have an id, using index this time */}
        {players.map((player,index) => <PlayerItem key={index} player={player}/>)}
    </div>
)};

export default PlayerList;