// todos/selectors.js
import { createSelector } from 'reselect';
import { NAME } from './constants';
import {filterVisible} from './model';

export const getAll = state => state[NAME];

export const getAllPlayers = state => state[NAME].players;

export const getVisiblePlayers = state => filterVisible(state[NAME].players,state[NAME].filters);

export const getFilters = state => state[NAME].filters;


export const getCounts = createSelector(
    getAll,
    // (allTodos, completedTodos, activeTodos) => ({
    //     all: allTodos.length,
    //     completed: completedTodos.length,
    //     active: activeTodos.length
    // })
);