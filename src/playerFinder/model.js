export const filterCompleted = todos => todos.filter(t => t.completed);

export const filterActive = todos => todos.filter(t => !t.completed);

const filter = {
    fieldName: "a field",
    value: ""
};

const matchFilters = (player, filters) => {
    let filterKeys = Object.keys(filters);
    for(let i=0;i<filterKeys.length;i++){
        let filter = filters[filterKeys[i]];
        if(filter.fieldName === "") continue;
        if( !player[filter.fieldName] || !player[filters[i].fieldName].contains(filter.value)) return false;
    }
    return true;
};

export const filterVisible = (players, filters) => players.filter(player => matchFilters(player,filters));