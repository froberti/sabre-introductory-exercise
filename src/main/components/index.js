import { connect } from 'react-redux';
import * as React from 'react';
import { createStructuredSelector } from 'reselect';
import {
    PlayerList,
    PlayerFilters,
    fetchPlayers,
    getVisiblePlayers,
    getFilters,
    filterPlayers
} from '../../playerFinder';

class Index extends React.Component{

  componentWillMount(){
      this.props.dispatch(fetchPlayers);
  }

  render() {
    const { players, filters, dispatch} = this.props;

    return (
      <div className="players-app">
        <PlayerFilters
            filters={filters}
            filterPlayer={(name,value) => dispatch(filterPlayers(name,value))}
        />
        <PlayerList
            players={players}
        />
      </div>
    );
  }
}
export default connect(
    createStructuredSelector({
        players: getVisiblePlayers,
        filters: getFilters
    })
)(Index);